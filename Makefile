#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/11 18:23:17 by pmartin           #+#    #+#              #
#    Updated: 2016/09/13 12:46:25 by pmartin          ###   ########.fr        #
#                                                                              #
#******************************************************************************#


NAME = Perlin
CC = cc
FLAGS = -Wall -Wextra -Werror
IDIR = includes/
LMLX = -framework OpenGL -framework AppKit -framework SDL2
LIBFT = libft/
SRC := $(filter %.c, $(shell find src -type f))
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	@make -C $(LIBFT)
	@$(CC) -o $@ $^ $(FLAGS) $(LIBFT)libft.a $(LMLX) -I $(IDIR)

%.o: %.c
	@$(CC) $(FLAGS) -o $@ -c $< -I $(IDIR)

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)
	@make -C $(LIBFT) fclean

re: fclean all

.PHONY: all $(NAME) clean fclean re