/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   perlin.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 13:31:28 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/15 12:47:30 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "perlin.h"

double		perlin_noise(double x, double y, double z)
{
	static t_grid		grid;
	t_perlin			perlin;
	int					i;
	int					j;

	i = 0;
	j = 0;
	if (!grid.o)
		init(&grid);
	int_double(x, &perlin.xyz[0]);
	int_double(y, &perlin.xyz[1]);
	int_double(z, &perlin.xyz[2]);
	perlin.uvw[0] = poly(perlin.xyz[0].doub);
	perlin.uvw[1] = poly(perlin.xyz[1].doub);
	perlin.uvw[2] = poly(perlin.xyz[2].doub);
	perlin.g00[0] = dot_product(grad(&grid, perlin.xyz[0].inte,
	perlin.xyz[1].inte,perlin.xyz[2].inte), perlin.xyz[0].doub,
	perlin.xyz[1].doub, perlin.xyz[2].doub);
	perlin.g00[1] = dot_product(grad(&grid, perlin.xyz[0].inte,
	perlin.xyz[1].inte, perlin.xyz[2].inte + 1), perlin.xyz[0].doub,
	perlin.xyz[1].doub , perlin.xyz[2].doub - 1.);
	perlin.g00[2] = dot_product(grad(&grid, perlin.xyz[0].inte,
	perlin.xyz[1].inte + 1, perlin.xyz[2].inte), perlin.xyz[0].doub,
	perlin.xyz[1].doub -1., perlin.xyz[2].doub);
	perlin.g00[3] = dot_product(grad(&grid, perlin.xyz[0].inte,
	perlin.xyz[1].inte + 1, perlin.xyz[2].inte + 1), perlin.xyz[0].doub,
	perlin.xyz[1].doub - 1., perlin.xyz[2].doub - 1.);
	perlin.g00[4] = dot_product(grad(&grid, perlin.xyz[0].inte + 1,
	perlin.xyz[1].inte, perlin.xyz[2].inte), perlin.xyz[0].doub -1.,
	perlin.xyz[1].doub, perlin.xyz[2].doub);
	perlin.g00[5] = dot_product(grad(&grid, perlin.xyz[0].inte + 1,
	perlin.xyz[1].inte, perlin.xyz[2].inte + 1), perlin.xyz[0].doub - 1.,
	perlin.xyz[1].doub, perlin.xyz[2].doub - 1.);
	perlin.g00[6] = dot_product(grad(&grid, perlin.xyz[0].inte + 1,
	perlin.xyz[1].inte + 1, perlin.xyz[2].inte), perlin.xyz[0].doub - 1.,
	perlin.xyz[1].doub - 1., perlin.xyz[2].doub);
	perlin.g00[7] = dot_product(grad(&grid, perlin.xyz[0].inte + 1,
	perlin.xyz[1].inte + 1, perlin.xyz[2].inte + 1), perlin.xyz[0].doub - 1.,
	perlin.xyz[1].doub -1., perlin.xyz[2].doub - 1.);
	perlin.x[0] = inter_li(perlin.g00[0], perlin.g00[4], perlin.uvw[0]);
	perlin.x[1] = inter_li(perlin.g00[2], perlin.g00[6], perlin.uvw[0]);
	perlin.x[2] = inter_li(perlin.g00[1], perlin.g00[5], perlin.uvw[0]);
	perlin.x[3] = inter_li(perlin.g00[3], perlin.g00[7], perlin.uvw[0]);
	perlin.y[0] = inter_li(perlin.x[0], perlin.x[1], perlin.uvw[1]);
	perlin.y[1] = inter_li(perlin.x[2], perlin.x[3], perlin.uvw[1]);
	perlin.z.doub = inter_li(perlin.y[0], perlin.y[1], perlin.uvw[2]);
	return (perlin.z.doub);
}

double	ezperlin(double x, double y, double z, t_config_perlin *conf)
{
	double	o;
	double	f;
	double	ampli;
	int 	i;

	i = 0;
	o = 0;
	f = conf->f;
	ampli = 1.0;
	while (i < conf->octave)
	{
		o += perlin_noise(x * f, y * f, z * f)
			* ampli;
		//printf("|%f| |%g|\n", o, o);
		ampli *= conf->persistence;
		f *= 2.0;
		i++;
	}
	f = (1.0 - conf->persistence) / (1.0 - ampli);

	return (o * f);
}
