/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   perlin_sdl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 11:12:30 by pmartin           #+#    #+#             */
/*   Updated: 2016/09/15 18:33:08 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "perlin.h"

void	make_color(int x, int y, t_var *var, t_color a)
{
	char *pix;

	pix = var->surface->pixels + (480 - y - 1)
		* var->surface->pitch + x * 4;
	pix[0] = (int)a.r;
	pix[1] = (int)a.g;
	pix[2] = (int)a.b;
}

void	make_perlin(t_var *var, double offset)
{
	double			o;
	double			j;
	double			i;
	t_color			a;
	t_color			b;
	t_config_perlin conf;

//bin/	i = var->width / 2 * offset;
	i = 0;
	conf.persistence = 0.01;
	conf.octave = 2;
	conf.f = 1;
	for (int x = 0; x < 640; x++){
//		j = -var->height / 2 * offset;
		j = 0;
		for (int y = 0; y < 480; y++)
		{
			for (int level = 0; level < 10; level++){
				o = (perlin_noise(i - ((var->width / 2) * offset),
								  j - ((var->height / 2) * offset),
								  3.2434) + 1) * 128;
			}
			make_color(x, y, var, a);
			j += offset;
		}
		i += offset;
	}
}

int main()
{
	t_var	var;
	int		end;
	double	k;
	double	kprev;

	k = 1;
	end = 1;
	var.height = 480;
	var.width = 640;
	var.x = 0;
	var.y = 0;
	if (SDL_Init(SDL_INIT_VIDEO))
		return (-1);
	var.sdl_win = SDL_CreateWindow("Perlin",
								   SDL_WINDOWPOS_UNDEFINED,
								   SDL_WINDOWPOS_UNDEFINED,
								   var.width,
								   var.height,
								   SDL_WINDOW_SHOWN);
	var.sdl_ren = SDL_CreateRenderer(var.sdl_win, -1,
								 SDL_RENDERER_ACCELERATED
								 | SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderDrawColor(var.sdl_ren, 0x00, 0x00, 0x00, 0x00);
	var.surface = SDL_CreateRGBSurface(0, var.width, var.height, 32,
								0, 0, 0, 0);
	make_perlin(&var, k);
	var.sdl_tex = SDL_CreateTextureFromSurface(var.sdl_ren, var.surface);
	SDL_RenderClear(var.sdl_ren);
	SDL_RenderCopy(var.sdl_ren, var.sdl_tex, 0, 0);
	SDL_RenderPresent(var.sdl_ren);
	while (end)
	{
		kprev = k;
		SDL_WaitEvent(&var.event);
		if (var.event.window.event == SDL_WINDOWEVENT_CLOSE
			|| var.event.type == SDL_QUIT
			|| var.event.key.keysym.sym == SDLK_ESCAPE)
			end = 0;
		if (var.event.key.keysym.sym == SDLK_RIGHT)
			k /= 10;
		else if (var.event.key.keysym.sym == SDLK_LEFT)
			k *= 10;
		else if (var.event.key.keysym.sym == SDLK_UP)
			k += k / 10;
		else if (var.event.key.keysym.sym == SDLK_DOWN)
			k -= k / 10;
		if (k != kprev)
		{
			printf("K == %f\n", k);
			make_perlin(&var, k);
			SDL_DestroyTexture(var.sdl_tex);
			var.sdl_tex = SDL_CreateTextureFromSurface(var.sdl_ren, var.surface);
			SDL_RenderClear(var.sdl_ren);
			SDL_RenderCopy(var.sdl_ren, var.sdl_tex, 0, 0);
			SDL_RenderPresent(var.sdl_ren);
		}
	}
	SDL_DestroyTexture(var.sdl_tex);
	SDL_DestroyWindow(var.sdl_win);
	SDL_Quit();
	return (1);
}
