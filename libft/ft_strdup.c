/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 22:45:53 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/22 15:37:48 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdup(char const *s)
{
	char	*s2;
	size_t	i;
	size_t	k;

	k = ft_strlen(s);
	i = 0;
	if (!(s2 = (char *)malloc(sizeof(*s) * ft_strlen(s) + 1)))
		return (0);
	while (i <= k)
	{
		s2[i] = s[i];
		i++;
	}
	s2[i] = '\0';
	return (s2);
}
