/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/17 16:52:04 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/23 12:35:21 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*list_temp;
	void	*tc;

	if (!(tc = ft_memalloc(sizeof(content_size))))
		return (0);
	if (!(list_temp = malloc(sizeof(*list_temp))))
		return (0);
	list_temp->content_size = content_size;
	if (content)
		ft_memcpy(tc, content, content_size);
	else
		tc = 0;
	list_temp->content = tc;
	list_temp->next = 0;
	return (list_temp);
}
