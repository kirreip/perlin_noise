/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 08:48:06 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/22 16:15:32 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *s1, const char *s2, size_t nb)
{
	size_t	*tab;

	tab = (size_t *)ft_strnew(7);
	tab[0] = 0;
	tab[1] = ft_strlen(s1);
	tab[2] = ft_strlen(s2);
	if (s1 == 0 || s2 == 0)
		return (0);
	while (nb > (tab[1] + tab[0] + 1))
	{
		s1[(tab[0]) + tab[1]] = s2[tab[0]];
		tab[0]++;
	}
	if (nb == tab[1] + 1 + tab[0])
		s1[tab[0] + tab[1] + 1] = '\0';
	if ((nb + tab[2]) < ft_strlen(s1) + tab[2])
		return (nb + tab[2]);
	else
		return (tab[1] + tab[2]);
}
