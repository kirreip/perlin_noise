/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 12:27:34 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/23 13:14:54 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *var;
	t_list *tmp;

	if (!lst)
		return (0);
	if (!(tmp = ft_lstnew(lst->content, lst->content_size)))
		return (0);
	tmp = f(lst);
	var = tmp;
	while (lst->next)
	{
		var->next = f(lst->next);
		var = var->next;
		lst = lst->next;
	}
	return (tmp);
}
