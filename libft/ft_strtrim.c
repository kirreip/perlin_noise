/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 00:34:17 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/23 10:48:16 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_spc(char c)
{
	return ((c == '\t' || c == '\n' || c == ' ') ? 1 : 0);
}

static int		ft_fullsp(char *s)
{
	int			i;
	int			k;

	i = 0;
	k = 1;
	while (s[i])
		if (!ft_spc(s[i++]))
			return (0);
	return (1);
}

static int		ft_lensp(char *s)
{
	int			i;
	int			k;

	k = ft_strlen(s);
	i = 0;
	while (ft_spc(s[i]))
		i++;
	while (ft_spc(s[i]))
		k--;
	return (i + (ft_strlen(s) - k) + 1);
}

char			*ft_strtrim(const char *s)
{
	char		*str;
	int			j;
	int			i;

	i = 0;
	j = ft_strlen(s) - 1;
	if (!s)
		return (0);
	if (!(str = ft_strnew(ft_lensp((char *)s))))
		return (0);
	if (!s[0] || ft_fullsp((char *)s))
		return (str);
	if (ft_spc(s[0]) == 0 && ft_spc(s[j]) == 0)
		return (ft_strdup(s));
	while (ft_spc(s[i]) || ft_spc(s[j]))
		(ft_spc(s[i])) ? i++ : j--;
	if (!(str = ft_strnew(j - i + 2)))
		return (0);
	return (ft_strncpy(str, &s[i], j - i + 1));
}
