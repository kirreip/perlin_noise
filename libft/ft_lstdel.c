/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/23 12:10:04 by pmartin           #+#    #+#             */
/*   Updated: 2016/07/23 13:14:27 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *var;

	var = *alst;
	if (!del || !alst)
		return ;
	while (var)
	{
		del(var->content, var->content_size);
		free(var);
		var = var->next;
	}
	*alst = 0;
}
